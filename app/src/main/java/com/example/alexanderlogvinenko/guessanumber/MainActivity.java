package com.example.alexanderlogvinenko.guessanumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;


/**
 * This application was developed by Alexander Logvinenko
 */
public class MainActivity extends AppCompatActivity {
    Random rand = new Random();
    int RandomNum = rand.nextInt(20)+1;

    public void Guess(View view){
        EditText guessInput = (EditText) findViewById(R.id.guessedAnswerField);


        try
        {
            int guessInputInt =  Integer.parseInt(guessInput.getText().toString());

            if(guessInputInt<RandomNum)
            {
                Toast.makeText(MainActivity.this, "Higher!", Toast.LENGTH_SHORT).show();
            }else if(guessInputInt>RandomNum){
                Toast.makeText(MainActivity.this, "Lower!", Toast.LENGTH_SHORT).show();
            }else if(guessInputInt==RandomNum){
                Toast.makeText(MainActivity.this, "number "+guessInputInt+" is correct. Lets try it again!", Toast.LENGTH_SHORT).show();
                RandomNum = rand.nextInt(20)+1;
            }
        }
        catch (NumberFormatException nfe)
        {
            Toast.makeText(MainActivity.this, "Please use numbers only.", Toast.LENGTH_SHORT).show();
            System.out.println("NumberFormatException: " + nfe.getMessage());
        }





    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
